Pod::Spec.new do |s|
  s.name             = 'HoopaSDK'
  s.version          = '2.6.0.2-beta'
  s.summary          = 'Hoopa framework for iBeacon support and push notifications, version 2.6.0.2-beta xcode 9.3.1'
  s.description      = <<-DESC
Hoopa's solution to proximity marketing adding support for iBeacon
                       DESC
  s.homepage         = 'http://hoopa.io'
  s.license          = { :type => 'Copyright',
    :text => <<-LICENSE
Copyright 2015 Hoopa. All rights reserved.
                LICENSE
    }
  s.author           = { 'Empatic' => 'contact@hoopa.io' }
  s.source           = { :git => 'https://hoopa_cmanterola@bitbucket.org/hoopa/release-fat.git', :tag => s.version.to_s }
  s.platform     = :ios
  s.ios.deployment_target = '8.2'

  s.vendored_frameworks = 'HoopaSDK.framework'
  s.dependency 'Firebase/Messaging','4.13'
  s.dependency 'Firebase/Core','4.13'

  s.frameworks = 'UIKit', 'Foundation', 'CoreData', 'CoreTelephony', 'CoreLocation', 'AVFoundation'
  s.requires_arc = true
  s.pod_target_xcconfig = { 'CODE_SIGNING_ALLOWED' => 'YES', 'CODE_SIGNING_REQUIRED' => 'YES' }

end
